#include "..\script_macros.hpp"
/*
    File: fn_initCop.sqf
    Author: Bryan "Tonic" Boardwine

    Description:
    Cop Initialization file.
*/
player addRating 9999999;
waitUntil {!(isNull (findDisplay 46))};

if (life_blacklisted) exitWith {
    ["Blacklisted",false,true] call BIS_fnc_endMission;
	closeDialog 1;// Add these line
	0 cutText["","BLACK FADED"];
    0 cutFadeOut 9999999;
    sleep 30;
};

if (!(str(player) in ["cop_1","cop_2","cop_3","cop_4"])) then {
    if ((FETCH_CONST(life_coplevel) isEqualTo 0) && (FETCH_CONST(life_adminlevel) isEqualTo 0)) then {
        closeDialog 1;// Add these line
		0 cutText["","BLACK FADED"];
		0 cutFadeOut 9999999;
		["NotWhitelisted",false,true] call BIS_fnc_endMission;
        sleep 35;
    };
};


player setVariable ["rank",(FETCH_CONST(life_coplevel)),true];
[] call life_fnc_spawnMenu;
waitUntil{!isNull (findDisplay 38500)}; //Wait for the spawn selection to be open.
waitUntil{isNull (findDisplay 38500)}; //Wait for the spawn selection to be done.

//_statement = {player, call life_coplevel},([] call life_fnc_DienstausweisLizenzen)};
//_condition = [playerSide == west && !isNull cursorTarget && cursorTarget isKindOf "Man"];
//_action = ["showBadge","Dienstausweis zeigen","",_statement, _condition];
//[cursorTarget, 0, ["ACE_MainActions"], _action] call ace_interact_menu_fnc_addActionToClass;


