#include "..\..\script_macros.hpp"

/*
fn_radArea.sqf

Created by Fuel for RebornRoleplay.com

[UK/EU] Reborn Roleplay | RebornRoleplay.co.uk
*/

_player = [_this,0,objNull,[objNull]] call BIS_fnc_param;
_damage = getDammage _player;
_uniform = uniform _player;
_vest = vest _player;
_goggles = goggles _player;
_rad1 = ppEffectCreate ["ChromAberration",600];
_rad2 = ppEffectCreate ["DynamicBlur",800];
_rad3 = ppEffectCreate ["FilmGrain",5000];

_rad1n = ppEffectCreate ["ChromAberration",100];
_rad2n = ppEffectCreate ["DynamicBlur",130];
_rad3n = ppEffectCreate ["FilmGrain",4];

_con = _player getVariable ["ACE_isUnconscious", false];

if (vehicle player != player) then {hint "Player is in a vehicle"};

if ( _con || !alive _player || (_damage == 1) || (_player getVariable ["exitRad",true])) exitWith {

	_rad1 ppEffectEnable false;
	_rad1 ppEffectAdjust [0,0,true];
	_rad1 ppEffectCommit 1;
	_rad2 ppEffectEnable false;
	_rad2 ppEffectAdjust [0];
	_rad2 ppEffectCommit 1;
	_rad3 ppEffectEnable false;
	_rad3 ppEffectAdjust [0,0,0,0,0,true];
	_rad3 ppEffectCommit 1;
	ppEffectDestroy [_rad1, _rad2, _rad3];
	
	_rad1n ppEffectEnable false;
	_rad1n ppEffectAdjust [0,0,true];
	_rad1n ppEffectCommit 1;
	_rad2n ppEffectEnable false;
	_rad2n ppEffectAdjust [0];
	_rad2n ppEffectCommit 1;
	_rad3n ppEffectEnable false;
	_rad3n ppEffectAdjust [0,0,0,0,0,true];
	_rad3n ppEffectCommit 1;
	ppEffectDestroy [_rad1n, _rad2n, _rad3n];
	hint "Out of Zone";
	};
	

// this is the check to see if they are wearing protective clothing
if ((_uniform isEqualTo "skn_u_nbc_opf_white") && (_vest isEqualTo "skn_o_elbv_worn_no_bp") && (_goggles isEqualTo "skn_m50_gas_mask_hood")) then {

//rad sound
	_player say2d "rad";
	hint "Triggered";

// edit this for the length of the effect
	for "_i" from 0 to 1 do {
	_rad1n ppEffectEnable true;
	_rad1n ppEffectAdjust [-0.02,0,true];
	_rad1n ppEffectCommit 1;
	_rad2n ppEffectEnable true;
	_rad2n ppEffectAdjust [0.03];
	_rad2n ppEffectCommit 1;
	_rad3n ppEffectEnable true;
	_rad3n ppEffectAdjust [0.12,1.52,3.54,2,2,true];
	_rad3n ppEffectCommit 1;
};
	sleep 5;

//loops the script until they exit
	[_player] remoteExec ["life_fnc_radArea",_player];

} else {

// edit this for the length of the effect
	for "_i" from 0 to 4 do {
	_rad1 ppEffectEnable true;
	_rad1 ppEffectAdjust [-0.02,0,true];
	_rad1 ppEffectCommit 1;
	_rad2 ppEffectEnable true;
	_rad2 ppEffectAdjust [0.03];
	_rad2 ppEffectCommit 1;
	_rad3 ppEffectEnable true;
	_rad3 ppEffectAdjust [0.12,1.52,3.54,2,2,true];
	_rad3 ppEffectCommit 1;
};
	sleep 5;
	
//set the damage to player
	 _var = missionNamespace getVariable "raddmg";
	if (isNil "_var") then
	{
		missionNamespace setVariable ["raddmg", 1]; 
		_var = 0.1;
	};
	_darray = ["body","body","body","head","head","hand_l","hand_r","leg_l","leg_r"];
	_random = selectRandom _darray;
	[player, _var, _random, "unknown"] call ace_medical_fnc_addDamageToUnit;
	_var = _var * ((floor random 2) + 1);
	missionNamespace setVariable ["raddmg",_var];


	

	
	
//loops the script until they exit
	[_player] remoteExec ["life_fnc_radArea",_player];
};