#include "..\..\script_macros.hpp"
/*
    File: fn_medicLoadout.sqf
    Author: Bryan "Tonic" Boardwine

    Description:
    Loads the medic out with the default gear.
*/
private ["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};

player addUniform "U_C_Paramedic_01_F";
player addVest "V_LegStrapBag_black_F";
player addHeadgear "H_Beret_blk";
player addBackpack "TFAR_anarc164";
player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "ItemGPS";
player linkItem "TFAR_anprc148jem";

[] call life_fnc_playerSkins;
[] call life_fnc_saveGear;
