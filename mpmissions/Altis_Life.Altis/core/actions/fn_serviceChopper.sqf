#include "..\..\script_macros.hpp"
/*
    File: fn_serviceChopper.sqf
    Author: Bryan "Tonic" Boardwine

    Description:
    Main functionality for the chopper service paid, to be replaced in later version.
*/
private ["_serviceCost"];
disableSerialization;
private ["_search","_ui","_progress","_cP","_pgText"];
if (life_action_inUse) exitWith {hint localize "STR_NOTF_Action"};

_serviceCost = LIFE_SETTINGS(getNumber,"service_chopper");
_search = nearestObjects[getPos air_sp, ["Air"],10];




if (count _search isEqualTo 0) exitWith {hint localize "STR_Service_Chopper_NoAir"};
if (CASH < _serviceCost) exitWith {hint localize "STR_Serive_Chopper_NotEnough"};

life_action_inUse = true;
[5,[_veh], {_this call inner_fnc_repair},{hint "Abbgebrochen"},"Repariere",{true},["default",0]] spawn rl_fnc_progressBar;
if (!alive (_search select 0) || (_search select 0) distance air_sp > 15) exitWith {life_action_inUse = false; hint localize "STR_Service_Chopper_Missing"};

inner_fnc_repair = {
  CASH = CASH - _serviceCost;
  if (!local (_search select 0)) then {
      [(_search select 0),1] remoteExecCall ["life_fnc_setFuel",(_search select 0)];
      } else {
      (_search select 0) setFuel 1;
      };
};

(_search select 0) setDamage 0;

"progressBar" cutText ["","PLAIN"];
titleText [localize "STR_Service_Chopper_Done","PLAIN"];
life_action_inUse = false;
