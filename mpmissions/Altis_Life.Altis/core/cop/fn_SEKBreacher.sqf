#include "..\..\script_macros.hpp"
/*
 File: fn_copLoadout.sqf
 Author: Bryan "Tonic" Boardwine
 Edited: Itsyuka
 Description:
 Loads the cops out with the default gear.
*/
if ((FETCH_CONST(life_coplevel) isEqualTo 0) && (FETCH_CONST(life_adminlevel) isEqualTo 0)) exitWith { hint "Du bist kein Polizeibeamter!" };
private["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};
//Load player with default cop gear.
player addUniform "U_I_G_Story_Protagonist_F";
player addVest "V_PlateCarrierSpec_blk";
player addItemToVest "ACE_M84";
player addItemToVest "HandGrenade";
player addBackpack "B_ViperLightHarness_blk_F";
player addItemToBackpack "ACE_EarPlugs";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addHeadgear "H_HelmetB_Enh_tna_F";
/* HANDGUN */
player addWeapon "CSW_FN57_Ballistic_Shield";
player addMagazine "CSW_20Rnd_57x28_SS190";
player addMagazine "CSW_20Rnd_57x28_SS190";
player addMagazine "CSW_20Rnd_57x28_SS190";
//player addMagazine "CSW_20Rnd_57x28_SS190";
//player addMagazine "CSW_20Rnd_57x28_SS190";
//player addMagazine "CSW_20Rnd_57x28_SS190";
player addHandgunItem "CSW_FN57_silencer3";
player addHandgunItem "CSW_FN57_flashlight_glare_2";
player addHandgunItem "CSW_FN57_Shield_P";
/* SCHARFE WAFFE */
player addWeapon "CSW_M870";
player addMagazine "CSW_M870_8Rnd_buck";
player addMagazine "CSW_M870_8Rnd_buck";
//player addMagazine "CSW_M870_8Rnd_buck";
//player addMagazine "CSW_M870_8Rnd_buck";
//player addMagazine "CSW_M870_8Rnd_buck";
//player addMagazine "CSW_M870_8Rnd_buck";
player addPrimaryWeaponItem "CSW_M870_flashlight_normal";



/* ITEMS */
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemWatch";
player assignItem "ItemWatch";
player addItem "ItemGPS";
player assignItem "ItemGPS";

[] call life_fnc_saveGear;