#include "..\..\script_macros.hpp"
/*
 File: fn_copLoadout.sqf
 Author: Bryan "Tonic" Boardwine
 Edited: Itsyuka
 Description:
 Loads the cops out with the default gear.
*/
if ((FETCH_CONST(life_coplevel) isEqualTo 0) && (FETCH_CONST(life_adminlevel) isEqualTo 0)) exitWith { hint "Du bist kein Polizeibeamter!" };
private["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};
//Load player with default cop gear.
player addUniform "U_O_T_Sniper_F";
player addItemToVest "ACE_DefusalKit";
player addVest "V_PlateCarrierSpec_blk";
player addBackpack "B_ViperLightHarness_blk_F";
player addItemToVest "SmokeShell";
player addItemToBackpack "ACE_EarPlugs";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addHeadgear "H_HelmetB_Enh_tna_F";
/* HANDGUN */
player addWeapon "CSW_FN57";
player addMagazine "CSW_20Rnd_57x28_SS190";
player addMagazine "CSW_20Rnd_57x28_SS190";
player addMagazine "CSW_20Rnd_57x28_SS190";
//player addMagazine "CSW_20Rnd_57x28_SS190";
//player addMagazine "CSW_20Rnd_57x28_SS190";
//player addMagazine "CSW_20Rnd_57x28_SS190";
player addHandgunItem "CSW_FN57_silencer3";
player addHandgunItem "CSW_FN57_flashlight_glare_2";
player addHandgunItem "optic_ACO_grn_smg";
/* SCHARFE WAFFE */
player addWeapon "srifle_EBR_F";
player addMagazine "20Rnd_762x51_Mag";
player addMagazine "20Rnd_762x51_Mag";
//player addMagazine "20Rnd_762x51_Mag";
//player addMagazine "20Rnd_762x51_Mag";
//player addMagazine "20Rnd_762x51_Mag";
//player addMagazine "20Rnd_762x51_Mag";
player addPrimaryWeaponItem "muzzle_snds_B";
player addPrimaryWeaponItem "ACE_acc_pointer_green";
player addPrimaryWeaponItem "optic_SOS";
player addPrimaryWeaponItem "bipod_01_F_blk";

/* ITEMS */
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemWatch";
player assignItem "ItemWatch";
player addItem "ItemGPS";
player assignItem "ItemGPS";

[] call life_fnc_saveGear;