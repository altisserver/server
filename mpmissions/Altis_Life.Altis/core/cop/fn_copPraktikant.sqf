#include "..\..\script_macros.hpp"
/*
 File: fn_copLoadout.sqf
 Author: Bryan "Tonic" Boardwine
 Edited: Itsyuka
 Description:
 Loads the cops out with the default gear.
*/
if ((FETCH_CONST(life_coplevel) isEqualTo 0) && (FETCH_CONST(life_adminlevel) isEqualTo 0)) exitWith { hint "Du bist kein Polizeibeamter!" };
private["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};
//Load player with default cop gear.
player addUniform "U_B_GEN_Soldier_F";
player addVest "V_PlateCarrier2_blk";
player addBackpack "B_FieldPack_blk";
player addItemToBackpack "ACE_EarPlugs";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addItemToBackpack "ACE_CableTie";
player addHeadgear "H_Cap_police";
/* HANDGUN */
player addWeapon "CSW_M26C";
player addMagazine "CSW_Taser_Probe_Mag";
player addMagazine "CSW_Taser_Probe_Mag";
player addMagazine "CSW_Taser_Probe_Mag";
//player addMagazine "CSW_Taser_Probe_Mag";
//player addMagazine "CSW_Taser_Probe_Mag";
//player addMagazine "CSW_Taser_Probe_Mag";
/* SCHARFE WAFFE */
//player addWeapon "arifle_SPAR_01_blk_F";
//player addMagazine "30Rnd_556x45_Stanag";
//player addMagazine "30Rnd_556x45_Stanag";
//player addMagazine "30Rnd_556x45_Stanag";
//player addMagazine "30Rnd_556x45_Stanag";
//player addMagazine "30Rnd_556x45_Stanag";
//player addMagazine "30Rnd_556x45_Stanag";
//player addPrimaryWeaponItem "optic_Holosight_blk_F";
/* ITEMS */
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemWatch";
player assignItem "ItemWatch";
player addItem "ItemGPS";
player assignItem "ItemGPS";

[] call life_fnc_saveGear;