#include "..\script_macros.hpp"
/*
    File: fn_initMedic.sqf
    Author: Bryan "Tonic" Boardwine

    Description:
    Initializes the medic..
*/
player addRating 99999999;
waitUntil {!(isNull (findDisplay 46))};
if (!(str(player) in ["medic_1"])) then {
    if (FETCH_CONST(life_medicLevel) isEqualTo 5) then {
        ["NotWhitelisted",false,true] call BIS_fnc_endMission;
		closeDialog 1;// Add these line
		0 cutText["","BLACK FADED"];
		0 cutFadeOut 9999999;
        sleep 35;
    };
};

if ((FETCH_CONST(life_medicLevel)) < 1 && (FETCH_CONST(life_adminlevel) isEqualTo 0)) exitWith {
    ["Notwhitelisted",false,true] call BIS_fnc_endMission;
	closeDialog 1;// Add these line
	0 cutText["","BLACK FADED"];
	0 cutFadeOut 9999999;
    sleep 35;
};

[] call life_fnc_spawnMenu;
waitUntil{!isNull (findDisplay 38500)}; //Wait for the spawn selection to be open.
waitUntil{isNull (findDisplay 38500)}; //Wait for the spawn selection to be done.


_action = ["VulcanPinch","Vulcan Pinch","",{[player, switch (playerSide) do {case west:{call life_coplevel}; case independent:{call life_medicLevel}; case east:{call life_adaclevel};},([] call life_fnc_DienstausweisLizenzen)] remoteExec ["life_fnc_DienstausweisZeigen",cursorObject];},{true},{},[parameters], [0,0,0], 100] call ace_interact_menu_fnc_createAction;
[cursorTarget, 0, ["ACE_MainActions"], _action] call ace_interact_menu_fnc_addActionToObject;