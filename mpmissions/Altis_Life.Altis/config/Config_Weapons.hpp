/*
*    FORMAT:
*        STRING (Conditions) - Must return boolean :
*            String can contain any amount of conditions, aslong as the entire
*            string returns a boolean. This allows you to check any levels, licenses etc,
*            in any combination. For example:
*                "call life_coplevel && license_civ_someLicense"
*            This will also let you call any other function.
*            
*
*    ARRAY FORMAT:
*        0: STRING (Classname): Item Classname
*        1: STRING (Nickname): Nickname that will appear purely in the shop dialog
*        2: SCALAR (Buy price)
*        3: SCALAR (Sell price): To disable selling, this should be -1
*        4: STRING (Conditions): Same as above conditions string
*
*    Weapon classnames can be found here: https://community.bistudio.com/wiki/Arma_3_CfgWeapons_Weapons
*    Item classnames can be found here: https://community.bistudio.com/wiki/Arma_3_CfgWeapons_Items
*
*/
class WeaponShops {
    //Armory Shops
    class gun {
        name = "Billy Joe's Firearms";
        side = "civ";
        conditions = "license_civ_gun";
        items[] = {
            { "hgun_Rook40_F", "", 10000, 5000, "" },
			{ "hgun_Pistol_heavy_01_MRD_F", "", 12000, 6000, "" },
			{ "hgun_ACPC2_F", "", 15000, 7500, "" },
			{ "hgun_P07_F", "", 10000, 5000, "" }
        };
        mags[] = {
            { "16Rnd_9x21_Mag", "", 250, 125, "" },
            { "9Rnd_45ACP_Mag", "", 500, 250, "" },
            { "30Rnd_9x21_Mag", "", 1000, 500, "" },
			{ "11Rnd_45ACP_Mag", "", 400, 200, "" }
        };
        accs[] = {
            { "optic_ACO_grn_smg", "", 1500, 750, "" },
			{ "acc_flashlight_pistol", "", 5000, 2500, "" },
			{ "muzzle_snds_L", "", 1500, 750, "" },
			{ "optic_Holosight", "", 1500, 750, "" }
        };
    };

    class rebel {
        name = "Mohammed's Jihadi Shop";
        side = "civ";
        conditions = "license_civ_rebel";
        items[] = {
            { "arifle_AK12_F", "", 300000, 150000,, "" },
            { "arifle_AKM_F", "", 250000, 125000, "" },
            { "arifle_Katiba_F", "", 200000, 100000, "" },
            { "arifle_AKS_F", "", 125000, 60000, "" },
            { "arifle_Katiba_C_F", "", 100000, 50000, "" }, 
            { "SMG_01_F ", "", 50000, 25000, "" }, 
            { "srifle_DMR_04_F", "", 500000, 250000, "" }
		};
        mags[] = {
            { "30Rnd_762x39_Mag_F", "", 2000, 1000, "" }, //alles mit AK 
            { "30Rnd_65x39_caseless_green", "", 1500, 750, "" }, //Katiba
			{ "30Rnd_45ACP_Mag_SMG_01", "", 500, 250, "" }, //.45 APC 30 kugeln
			{ "10Rnd_127x54_Mag", "", 3000, 1500, "" } //12,7mm ASP-1Kir

        };
        accs[] = {
            { "optic_ACO_grn_smg", "", 950, 475, "" },
			{ "acc_flashlight_smg_01", "", 950, 475, "" },
			{ "optic_Holosight_smg", "", 950, 475, "" },
			{ "optic_MRCO", "", 950, 475, "" },
			{ "muzzle_snds_acp", "", 950, 475, "" },
			{ "optic_Holosight_smgg", "", 950, 475, "" },
			{ "	optic_Hamr", "", 950, 475, "" },
			{ "	muzzle_snds_M", "", 5000, 2500, "" },
			{ "	muzzle_snds_L", "", 5000, 2500, "" },
			{ "	muzzle_snds_B", "", 5000, 2500, "" },
			{ "	muzzle_snds_H", "", 5000, 2500, "" },
			{ "	optic_Aco", "", 950, 475, "" },
			{ "	optic_SOS", "", 950, 475, "" },
			{ "	bipod_01_F_blk", "", 950, 475, "" },
			{ "	bipod_02_F_blk", "", 950, 475, "" },
			{ "	bipod_03_F_blk", "", 950, 475, "" }
        };
    };

    class gang {
        name = "Hideout Armament";
        side = "civ";
        conditions = "";
        items[] = {

            { "hgun_PDW2000_F", "", 40000, 20000, "" },
			{ "SMG_05_F", "", 35000, 17500, "" },
			{ "SMG_02_F", "", 40000, 20000, "" },
			{ "arifle_Mk20_F", "", 175000, 100000, "" },
			{ "arifle_TRG21_F", "", 200000, 100000, "" },
			{ "arifle_TRG20_F", "", 175000, 100000, "" },
			{ "hgun_Pistol_heavy_02_F", "", 15000, 7500, "" }
        };
        mags[] = {
            { "30Rnd_9x21_Mag", "", 1000, 500, "" }, //PDW, MP5, Stinger
			{ "6Rnd_45ACP_Cylinder", "", 500, 250, "" }, //Zubr
			{ "30Rnd_556x45_Stanag", "", 1500, 750, "" } //TRG, MK20, 
		};
        accs[] = {
            { "optic_ACO_grn_smg", "", 950, 475, "" },
			{ "acc_flashlight_smg_01", "", 950, 475, "" },
			{ "optic_Holosight_smg", "", 950, 475, "" },
			{ "optic_MRCO", "", 950, 475, "" },
			{ "muzzle_snds_acp", "", 950, 475, "" },
			{ "optic_Holosight_smgg", "", 950, 475, "" },
			{ "	optic_Hamr", "", 950, 475, "" },
			{ "	muzzle_snds_M", "", 5000, 2500, "" },
			{ "	muzzle_snds_L", "", 5000, 2500, "" },
			{ "	muzzle_snds_B", "", 5000, 2500, "" },
			{ "	muzzle_snds_H", "", 5000, 2500, "" },
			{ "	optic_Aco", "", 950, 475, "" },
			{ "	optic_SOS", "", 950, 475, "" },
			{ "	bipod_01_F_blk", "", 950, 475, "" },
			{ "	bipod_02_F_blk", "", 950, 475, "" },
			{ "	bipod_03_F_blk", "", 950, 475, "" }
        };
    };

    //Basic Shops
    class genstore {
        name = "Altis General Store";
        side = "civ";
        conditions = "";
        items[] = {
            { "Binocular", "", 150, 75, "" },
            { "ItemGPS", "", 100, 50, "" },
            { "ItemMap", "", 50, 25, "" },
            { "ItemCompass", "", 50, 25, "" },
            { "ItemWatch", "", 50, 25, "" },
            { "FirstAidKit", "", 150, 75, "" },
            { "NVGoggles", "", 2000, 1000, "" },
            { "Chemlight_red", "", 300, 150, "" },
            { "Chemlight_yellow", "", 300, 150, "" },
            { "Chemlight_green", "", 300, 150, "" },
            { "Chemlight_blue", "", 300, 150, "" }
        };
        mags[] = {};
        accs[] = {};
    };

    class f_station_store {
        name = "Altis Fuel Station Store";
        side = "";
        conditions = "";
        items[] = {
            { "Binocular", "", 750, 75, "" },
            { "ItemGPS", "", 500, 50, "" },
            { "ItemMap", "", 250, 25, "" },
            { "ItemCompass", "", 250, 25, "" },
            { "ItemWatch", "", 250, 25, "" },
            { "FirstAidKit", "", 750, 75, "" },
            { "NVGoggles", "", 10000, 1000, "" },
            { "Chemlight_red", "", 1500, 150, "" },
            { "Chemlight_yellow", "", 1500, 150, "" },
            { "Chemlight_green", "", 1500, 150, "" },
            { "Chemlight_blue", "", 1500, 150, "" }
        };
        mags[] = {};
        accs[] = {};
    };

    //Cop Shops
    class cop_basic {
        name = "Altis Cop Shop";
        side = "cop";
        conditions = "";
        items[] = {
            { "Binocular", "", 150, 75, "" },
            { "ItemGPS", "", 100, 50, "" },
            { "FirstAidKit", "", 150, 75, "" },
            { "NVGoggles", "", 2000, 1000, "" },
			{ "ItemMap", "", 150, 75, ""},
			{ "ItemCompass", "", 150, 75, ""},
			{ "ItemWatch", "", 150, 75, ""},
            { "HandGrenade_Stone", $STR_W_items_Flashbang, 1700, 850, "Call life_coplevel >=3" },
			{ "HandGrenade", "", 7500, 3250, "Call life_coplevel >=5"},
			{ "SmokeShell", "", 750, 375, "call life_coplevel >=5"},			
			{ "ACE_M84", "", 3000, 1500, "Call life_coplevel >=5"},
			{ "ACE_EarPlugs", "", 50, 25, ""},
			{ "ACE_CableTie", "", 50, 25, ""},
			{ "ACE_DefusalKit", "", 400, 400, "Call life_coplevel >=5"},
			{ "srifle_EBR_F", "", 50000, 50000, "Call life_coplevel >=5"},			
			{ "arifle_MX_Black_F", "", 1500, 1500, "Call life_coplevel >=5"},						
			{ "CSW_FN57", "", 7500, 7500, "Call life_coplevel >=5"},
			{ "CSW_FN57_Ballistic_Shield", "", 10000, 10000, "Call life_coplevel >=5"},				
			{ "CSW_M870", "", 15000, 15000, "Call life_coplevel >=5"},			
			{ "hgun_Pistol_heavy_01_F", "", 7500, 7500, "call life_coplevel >=2"},		
			{ "arifle_SPAR_01_blk_F", "", 15000, 15000, "call life_coplevel >=2"},		
			{ "CSW_M26C", "", 1500, 750, ""},
			{ "CSW_M870_flashlight_normal", "", 750, 375, "Call life_coplevel >= 5"},
			{ "CSW_FN57_Shield_P", "", 500, 250, "Call life_coplevel >=5"},
			{ "CSW_FN57_flashlight_glare_2", "", 500, 250, "Call life_coplevel >=5"},
			{ "CSW_FN57_silencer3", "", 2000, 1000, "Call life_coplevel >=5"}
        };
        mags[] = {
            { "16Rnd_9x21_Mag", "", 125, 60, "" },
			{ "CSW_Taser_Probe_Mag", "", 50, 30, ""},
            { "20Rnd_556x45_UW_mag", $STR_W_mags_TaserRifle, 125, 60, "" },
            { "11Rnd_45ACP_Mag", "", 130, 65, "call life_coplevel >= 1" },
            { "30Rnd_65x39_caseless_mag", "", 130, 65, "call life_coplevel >= 2" },
            { "30Rnd_9x21_Mag", "", 250, 125, "call life_coplevel >= 2" },
            { "9Rnd_45ACP_Mag", "", 200, 100, "call life_coplevel >= 3" },
			{ "CSW_M870_8Rnd_buck", "", 5000, 5000, "Call life_coplevel >=5"},
			{ "CSW_20Rnd_57x28_SS190", "", 2500, 2500, "Call life_coplevel >=5"},
            { "20Rnd_650x39_Cased_Mag_F", "", 200, 100, "call life_coplevel >= 3" }
			
			//Apex DLC
        };
        accs[] = {
            { "muzzle_snds_L", "", 650, 325, "" },
            { "optic_MRD", "", 2750, 1375, "call life_coplevel >= 1" },
            { "acc_flashlight", "", 750, 375, "call life_coplevel >= 2" },
            { "optic_Holosight", "", 1200, 600, "call life_coplevel >= 2" },
            { "optic_Arco", "", 2500, 1250, "call life_coplevel >= 2" },
            { "muzzle_snds_H", "", 2750, 1375, "call life_coplevel >= 2" }
			
        };
    };

    //Medic Shops
    class med_basic {
        name = "Alles fÃ¼r den Medic";
        side = "med";
        conditions = "";
        items[] = {
			{ "DaXp_ACE_Defibrillator", "", 150, 50, ""},
			{ "TFAR_anprc148jem", "", 450, 50, ""},
			{ "I_UavTerminal", "UAV Terminal", 500, 50, ""},
			{ "ACE_key_master", "ZentralschlÃ¼ssel", 10, 5, ""},
            { "ItemGPS", "", 100, 50, "" },
            { "Binocular", "", 150, 75, "" },
            { "FirstAidKit", "", 150, 75, "" },
            { "ACE_adenosine", "", 1200, 10, "" },
			{ "ACE_atropine", "", 1200, 10, "" },
			{ "ACE_fieldDressing", "", 10, 10, "" },
			{ "ACE_elasticBandage", "", 10, 10, "" },
			{ "ACE_SpraypaintBlue", "", 10, 10, "" },
			{ "ACE_bloodIV", "", 10, 10, "" },
			{ "ACE_bloodIV_250", "", 10, 10, "" },
			{ "ACE_bloodIV_500", "", 10, 10, "" },
			{ "ACE_epinephrine", "", 10, 10, "" },
			{ "ACE_SpraypaintGreen", "", 10, 10, "" },
			{ "ACE_CableTie", "", 10, 10, "" },
			{ "ACE_MapTools", "", 10, 10, "" },
			{ "ACE_morphine", "", 10, 10, "" },
			{ "ACE_packingBandage", "", 10, 10, "" },
			{ "ACE_EarPlugs", "", 10, 10, "" },
			{ "ACE_surgicalKit", "", 10, 10, "" },
			{ "ACE_quikclot", "", 10, 10, "" },
			{"ACE_personalAidKit", "", 10, 10, ""}
			
        };
        mags[] = {};
        accs[] = {};
    };
};