class pro_loading
{
	idd = 38500;
	name = "pro_loading";
    fadein = 0;
    duration = 99999999999;
    fadeout = 1;
    movingEnable = 0;
    enableSimulation = 1;
	onLoad = "uiNamespace setVariable ['pro_loading', _this select 0]";
	
	class controlsBackground
	{
		class Map : Life_RscMapControl {
			idc = 38502;
			
			text = "";
			x = 0.0 * safezoneW + safezoneX;
			y = 0.0 * safezoneH + safezoneY;
			w = 1.0 * safezoneW;
			h = 1.0 * safezoneH;
			
			maxSatelliteAlpha = 1;
			alphaFadeStartScale = 1.15;
			alphaFadeEndScale = 1.29;
		};
	};
	
	class controls
	{
		class listBoxBackground: Life_RscStructuredText
		{
			idc = 1200;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.786 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.165 * safezoneH;
			colorBackground[] = {-1,-1,-1,0.3};
		};
		
		class SpawnPointList: Life_RscListNBox {
            idc = 38510;
            text = "";
            sizeEx = 0.041;
            coloumns[] = {0,0,0.9};
            drawSideArrows = 0;
            idcLeft = -1;
            idcRight = -1;
            rowHeight = 0.050;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.786 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.165 * safezoneH;
            onLBSelChanged = "_this call life_fnc_spawnPointSelected;";
        };
		
		class ButtonContinue: Life_RscButtonMenu
		{
			idc = 2400;
			
			text = "$STR_Spawn_Spawn";
			x = 0.897031 * safezoneW + safezoneX;
			y = 0.962 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.022 * safezoneH;
			
            onButtonClick = "[] call life_fnc_spawnConfirm";
			colorBackground[] = {-1,-1,-1,0.7};
			//font = FontM;
		};
		
		class ButtonBack: Life_RscButtonMenu
		{
			idc = -1;
			
			text = "$STR_lloading_exit"; 
			x = 0.0101562 * safezoneW + safezoneX;
			y = 0.962 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.022 * safezoneH;
			
			//font = FontM;
			colorBackground[] = {-1,-1,-1,0.7};
            onButtonClick = "failMission ""Zur Lobby""; closeDialog 0;";
		};
		
		class Info_Links: Life_RscStructuredText
		{
			idc = 1100;

			x = 0.0101556 * safezoneW + safezoneX;
			y = 0.181 * safezoneH + safezoneY;
			w = 0.221719 * safezoneW;
			h = 0.77 * safezoneH;
			//colorBackground[] = {-1,-1,-1,0.1};
		};
		
		class Info_Rechts: Life_RscStructuredText
		{
			idc = 1101;

			x = 0.768125 * safezoneW + safezoneX;
			y = 0.181 * safezoneH + safezoneY;
			w = 0.221719 * safezoneW;
			h = 0.77 * safezoneH;
			//colorBackground[] = {-1,-1,-1,0.1};
		};
		
		class Info_Titel: Life_RscStructuredText
		{
			idc = 1102;
			x = 0.0101556 * safezoneW + safezoneX;
			y = 0.00500001 * safezoneH + safezoneY;
			w = 0.979687 * safezoneW;
			h = 0.165 * safezoneH;
			//colorBackground[] = {-1,-1,-1,0.1};
		};
		
		class ProgressBar : Life_RscProgress {
            idc = 44201;

			x = 0.108125 * safezoneW + safezoneX;
			y = 0.962 * safezoneH + safezoneY;
			w = 0.78375 * safezoneW;
			h = 0.022 * safezoneH;
			
			colorBackground[] = { 0, 0, 0, 0.5};
			colorFrame[] = { 0, 0, 0, 0.8 };
        };

        class ProgressText : Life_RscStructuredText {
            idc = 38501;
			
            text = "$STR_Spawn_Title";
			x = 0.108125 * safezoneW + safezoneX;
			y = 0.962 * safezoneH + safezoneY;
			w = 0.78375 * safezoneW;
			h = 0.022 * safezoneH;
			
			class Attributes {
				align = "center";
			};
        };
	};
};