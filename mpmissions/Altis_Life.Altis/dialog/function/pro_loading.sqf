#include "..\..\script_macros.hpp"
/*
	File: pro_loading.sqf
    Author: Leon "DerL30N" Beeser

    Description:
    Setting up dialog and some display stuff...

*/
waitUntil {!isNull player && player == player}; //Wait till the player is ready

disableSerialization;
sleep 0.1;

createDialog "pro_loading";

LDGBSE displaySetEventHandler ["keyDown","_this call life_fnc_displayHandler"];

waitUntil {!isNull LDGBSE};

LDGEXT(false); CONTROL(38500,38510) ctrlShow false; CONTROL(38500,1200) ctrlShow false;

call life_fnc_proLoadingText;