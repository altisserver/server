#include "..\..\script_macros.hpp"
/*
	File: fn_proLoadingText.sqf
    Author: Leon "DerL30N" Beeser

    Description:
    Setting up some text...

*/
private _ctrla = LDGBSE displayCtrl 1100;
private _ctrlb = LDGBSE displayCtrl 1101;
private _ctrlc = LDGBSE displayCtrl 1102;
private _msga = "
		<t size='1.8'>Infos zur Einreise</t><br />
		<br />
		<br />
		<t color='#767676'><t size='1.6'>Projekt Leiter</t></t><br/>
		Timo <br />
		Aaron <br />
		<br />
		<br />
		<br />
		<t color='#767676'><t size='1.6'>Moderatoren</t></t><br/>
		Werden noch gesucht! <br />
		<br />
		<br />
		<br />
		<t color='#767676'><t size='1.6'>Teamspeak3</t></t><br/>
		Ts3 IP: ts.parabellum-life.de <br />
		
		<br />
		<br />
		<t color='#767676'><t size='1.6'>Schneller Support</t></t><br/>
		Gibt´s im Teamspeak3! <br />
		<br />
		<br />
		<br />
		<t color='#767676'><t size='2.1'>Wir wünschen euch viel Spaß!</t></t><br />
		Wenn Fragen aufkommen steht dir der Support<br />
		gerne zu Verfügung <br />
";
private _msgb = "
		<t size='1.8' align=right><t color='#ff0000'>Unsere Server Features</t></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right>Dynamisches Markt System</t>
		<br /><t size='1.1' align=right>Verwaltungssystem</t>
		<br /><t size='1.1' align=right>AAN News</t>
		<br /><t size='1.1' align=right>GPS System</t>
		<br /><t size='1.1' align=right>Dynamischer Spritverbrauch</t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br/>
		<br/><t color='#767676' align=right><t size='1.4'>Extra Featers von Uns für Euch</t></t>
		<br /><t size='1.1' align=right>Extrem schneller Support</t>
		<br /><t size='1.1' align=right>Aktives Serverteam</t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
		<br /><t size='1.1' align=right></t>
";
private _msgc = "<br /><br /><br />    <t color='#000000' align=center><t size='3.5'>Willkommen auf ParaBellum Life</t></t><br/>";

_ctrla ctrlSetStructuredText (parseText _msga);
_ctrlb ctrlSetStructuredText (parseText format["%1",_msgb]);
_ctrlc ctrlSetStructuredText (parseText _msgc);