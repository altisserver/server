#include "..\..\script_macros.hpp"
/*
	File: fn_proLoading.sqf
    Author: Leon "DerL30N" Beeser

    Description:
    Setting up some display stuff...

*/
LDGEXT(true); CONTROL(38500,38510) ctrlShow true; CONTROL(38500,1200) ctrlShow true; LDGBAR(1);
LDGTXT(localize "STR_Spawn_Title");

call life_fnc_proLoadingText;