// ACE Allgemein
force ace_common_allowFadeMusic = true;
force ace_common_checkPBOsAction = 2;
force ace_common_checkPBOsCheckAll = false;
force ace_common_checkPBOsWhitelist = "[]";
force ace_common_displayTextColor = [0,0,0,0.1];
force ace_common_displayTextFontColor = [1,1,1,1];
force ace_common_settingFeedbackIcons = 1;
force ace_common_settingProgressBarLocation = 0;
force ace_noradio_enabled = true;
force ace_parachute_hideAltimeter = true;

// ACE AuÃŸenlaststationen
force ace_pylons_enabledForZeus = true;
force ace_pylons_enabledFromAmmoTrucks = true;
force ace_pylons_rearmNewPylons = false;
force ace_pylons_requireEngineer = false;
force ace_pylons_requireToolkit = true;
force ace_pylons_searchDistance = 15;
force ace_pylons_timePerPylon = 5;

// ACE BenutzeroberflÃ¤che
force ace_ui_allowSelectiveUI = true;
force ace_ui_ammoCount = false;
force ace_ui_ammoType = true;
force ace_ui_commandMenu = true;
force ace_ui_firingMode = true;
force ace_ui_groupBar = false;
force ace_ui_gunnerAmmoCount = true;
force ace_ui_gunnerAmmoType = true;
force ace_ui_gunnerFiringMode = true;
force ace_ui_gunnerLaunchableCount = true;
force ace_ui_gunnerLaunchableName = true;
force ace_ui_gunnerMagCount = true;
force ace_ui_gunnerWeaponLowerInfoBackground = true;
force ace_ui_gunnerWeaponName = true;
force ace_ui_gunnerWeaponNameBackground = true;
force ace_ui_gunnerZeroing = true;
force ace_ui_magCount = true;
force ace_ui_soldierVehicleWeaponInfo = true;
force ace_ui_staminaBar = false;
force ace_ui_stance = true;
force ace_ui_throwableCount = true;
force ace_ui_throwableName = true;
force ace_ui_vehicleAltitude = true;
force ace_ui_vehicleCompass = true;
force ace_ui_vehicleDamage = true;
force ace_ui_vehicleFuelBar = true;
force ace_ui_vehicleInfoBackground = true;
force ace_ui_vehicleName = true;
force ace_ui_vehicleNameBackground = true;
force ace_ui_vehicleRadar = true;
force ace_ui_vehicleSpeed = true;
force ace_ui_weaponLowerInfoBackground = true;
force ace_ui_weaponName = true;
force ace_ui_weaponNameBackground = true;
force ace_ui_zeroing = true;

// ACE DurchzÃ¼ndung
force ace_cookoff_ammoCookoffDuration = 1;
force ace_cookoff_enable = false;
force ace_cookoff_enableAmmobox = true;
force ace_cookoff_enableAmmoCookoff = true;
force ace_cookoff_probabilityCoef = 1;

// ACE Einheitenwechsel
force ace_switchunits_enableSafeZone = true;
force ace_switchunits_enableSwitchUnits = false;
force ace_switchunits_safeZoneRadius = 100;
force ace_switchunits_switchToCivilian = false;
force ace_switchunits_switchToEast = false;
force ace_switchunits_switchToIndependent = false;
force ace_switchunits_switchToWest = false;

// ACE Erweiterte Ausdauer
force ace_advanced_fatigue_enabled = true;
force ace_advanced_fatigue_enableStaminaBar = true;
force ace_advanced_fatigue_loadFactor = 1;
force ace_advanced_fatigue_performanceFactor = 2.14646;
force ace_advanced_fatigue_recoveryFactor = 1.5;
force ace_advanced_fatigue_swayFactor = 0.5;
force ace_advanced_fatigue_terrainGradientFactor = 1;

// ACE Erweiterte Ballistik
force ace_advanced_ballistics_ammoTemperatureEnabled = true;
force ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
force ace_advanced_ballistics_bulletTraceEnabled = true;
force ace_advanced_ballistics_enabled = true;
force ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
force ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Erweitertes Wurfsystem
force ace_advanced_throwing_enabled = true;
force ace_advanced_throwing_enablePickUp = true;
force ace_advanced_throwing_enablePickUpAttached = true;
force ace_advanced_throwing_showMouseControls = true;
force ace_advanced_throwing_showThrowArc = true;

// ACE Fahrzeugsperre
force ace_vehiclelock_defaultLockpickStrength = 10;
force ace_vehiclelock_lockVehicleInventory = true;
force ace_vehiclelock_vehicleStartingLockState = 1;

// ACE Fingerzeig
force ace_finger_enabled = true;
force ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
force ace_finger_indicatorForSelf = true;
force ace_finger_maxRange = 4;

// ACE Gefangene
force ace_captives_allowHandcuffOwnSide = true;
force ace_captives_allowSurrender = true;
force ace_captives_requireSurrender = 2;
force ace_captives_requireSurrenderAi = false;

// ACE GehÃ¶r
force ace_hearing_autoAddEarplugsToUnits = true;
force ace_hearing_disableEarRinging = false;
force ace_hearing_earplugsVolume = 0.332739;
force ace_hearing_enableCombatDeafness = true;
force ace_hearing_enabledForZeusUnits = true;
force ace_hearing_unconsciousnessVolume = 0.11735;

// ACE HeiÃŸlaufen
force ace_overheating_displayTextOnJam = true;
force ace_overheating_enabled = true;
force ace_overheating_overheatingDispersion = true;
force ace_overheating_showParticleEffects = true;
force ace_overheating_showParticleEffectsForEveryone = false;
force ace_overheating_unJamFailChance = 0.05;
force ace_overheating_unJamOnreload = false;

// ACE Interaktion
ace_interaction_disableNegativeRating = false;
ace_interaction_enableMagazinePassing = true;
force ace_interaction_enableTeamManagement = false;

// ACE InteraktionsmenÃ¼
force ace_gestures_showOnInteractionMenu = 2;
force ace_interact_menu_actionOnKeyRelease = true;
force ace_interact_menu_addBuildingActions = false;
force ace_interact_menu_alwaysUseCursorInteraction = false;
force ace_interact_menu_alwaysUseCursorSelfInteraction = false;
force ace_interact_menu_colorShadowMax = [0,0,0,1];
force ace_interact_menu_colorShadowMin = [0,0,0,0.25];
force ace_interact_menu_colorTextMax = [1,1,1,1];
force ace_interact_menu_colorTextMin = [1,1,1,0.25];
force ace_interact_menu_cursorKeepCentered = false;
force ace_interact_menu_menuAnimationSpeed = 0;
force ace_interact_menu_menuBackground = 0;
force ace_interact_menu_selectorColor = [1,0,0];
force ace_interact_menu_shadowSetting = 2;
force ace_interact_menu_textSize = 2;
force ace_interact_menu_useListMenu = false;

// ACE Karte
force ace_map_BFT_Enabled = false;
force ace_map_BFT_HideAiGroups = false;
force ace_map_BFT_Interval = 1;
force ace_map_BFT_ShowPlayerNames = false;
force ace_map_DefaultChannel = -1;
force ace_map_mapGlow = true;
force ace_map_mapIllumination = true;
force ace_map_mapLimitZoom = false;
force ace_map_mapShake = true;
force ace_map_mapShowCursorCoordinates = false;
force ace_markers_moveRestriction = 0;

// ACE Kartenwerkzeug
force ace_maptools_drawStraightLines = true;
force ace_maptools_rotateModifierKey = 1;

// ACE Kartenzeichen
force ace_map_gestures_defaultColor = [1,0.88,0,0.7];
force ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
force ace_map_gestures_enabled = true;
force ace_map_gestures_interval = 0.03;
force ace_map_gestures_maxRange = 7;
force ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];

// ACE Logistik
force ace_cargo_enable = true;
force ace_cargo_paradropTimeCoefficent = 2.5;
force ace_rearm_level = 0;
force ace_rearm_supply = 0;
force ace_refuel_hoseLength = 12;
force ace_refuel_rate = 1;
force ace_repair_addSpareParts = true;
force ace_repair_autoShutOffEngineWhenStartingRepair = false;
force ace_repair_consumeItem_toolKit = 0;
force ace_repair_displayTextOnRepair = true;
force ace_repair_engineerSetting_fullRepair = 2;
force ace_repair_engineerSetting_repair = 1;
force ace_repair_engineerSetting_wheel = 0;
force ace_repair_fullRepairLocation = 1;
force ace_repair_repairDamageThreshold = 0.6;
force ace_repair_repairDamageThreshold_engineer = 0.4;
force ace_repair_wheelRepairRequiredItems = 1;

// ACE Magazine Repack
force ace_magazinerepack_timePerAmmo = 1.5;
force ace_magazinerepack_timePerBeltLink = 8;
force ace_magazinerepack_timePerMagazine = 2;

// ACE Markierungssystem (Spraydose)
force ace_tagging_quickTag = 1;

// ACE Mk6 MÃ¶rser
force ace_mk6mortar_airResistanceEnabled = false;
force ace_mk6mortar_allowCompass = true;
force ace_mk6mortar_allowComputerRangefinder = true;
force ace_mk6mortar_useAmmoHandling = false;

// ACE Respawn
force ace_respawn_removeDeadBodiesDisconnected = true;
force ace_respawn_savePreDeathGear = false;

// ACE SanitÃ¤ter
force ace_medical_ai_enabledFor = 0;
force ace_medical_AIDamageThreshold = 1;
force ace_medical_allowLitterCreation = true;
force ace_medical_allowUnconsciousAnimationOnTreatment = false;
force ace_medical_amountOfReviveLives = -1;
force ace_medical_bleedingCoefficient = 1;
force ace_medical_blood_enabledFor = 2;
force ace_medical_consumeItem_PAK = 0;
force ace_medical_consumeItem_SurgicalKit = 0;
force ace_medical_delayUnconCaptive = 3;
force ace_medical_enableAdvancedWounds = true;
force ace_medical_enableFor = 0;
force ace_medical_enableOverdosing = true;
force ace_medical_enableRevive = 1;
force ace_medical_enableScreams = true;
force ace_medical_enableUnconsciousnessAI = 1;
force ace_medical_enableVehicleCrashes = true;
force ace_medical_healHitPointAfterAdvBandage = true;
force ace_medical_increaseTrainingInLocations = false;
force ace_medical_keepLocalSettingsSynced = true;
force ace_medical_level = 2;
force ace_medical_litterCleanUpDelay = 240;
force ace_medical_litterSimulationDetail = 1;
force ace_medical_maxReviveTime = 1200;
force ace_medical_medicSetting = 2;
force ace_medical_medicSetting_basicEpi = 1;
force ace_medical_medicSetting_PAK = 1;
force ace_medical_medicSetting_SurgicalKit = 1;
force ace_medical_menu_allow = 1;
force ace_medical_menu_maxRange = 3;
force ace_medical_menu_openAfterTreatment = true;
force ace_medical_menu_useMenu = 1;
force ace_medical_menuTypeStyle = 0;
force ace_medical_moveUnitsFromGroupOnUnconscious = false;
force ace_medical_painCoefficient = 4.31966;
force ace_medical_painEffectType = 0;
force ace_medical_painIsOnlySuppressed = true;
force ace_medical_playerDamageThreshold = 2.98351;
force ace_medical_preventInstaDeath = true;
force ace_medical_remoteControlledAI = false;
force ace_medical_useCondition_PAK = 1;
force ace_medical_useCondition_SurgicalKit = 1;
force ace_medical_useLocation_basicEpi = 4;
force ace_medical_useLocation_PAK = 4;
force ace_medical_useLocation_SurgicalKit = 3;

// ACE Schnellzugang
force ace_quickmount_distance = 3;
force ace_quickmount_enabled = false;
force ace_quickmount_priority = 0;
force ace_quickmount_speed = 18;

// ACE Schutzbrille
force ace_goggles_effects = 2;
force ace_goggles_showInThirdPerson = false;

// ACE Sichtweitenbegrenzung
force ace_viewdistance_enabled = true;
force ace_viewdistance_limitViewDistance = 10000;
force ace_viewdistance_objectViewDistanceCoeff = 0;
force ace_viewdistance_viewDistanceAirVehicle = 0;
force ace_viewdistance_viewDistanceLandVehicle = 0;
force ace_viewdistance_viewDistanceOnFoot = 0;

// ACE Splittersimulation
force ace_frag_enabled = true;
force ace_frag_maxTrack = 10;
force ace_frag_maxTrackPerFrame = 10;
force ace_frag_reflectionsEnabled = false;
force ace_frag_spallEnabled = false;

// ACE Sprengstoffe
force ace_explosives_explodeOnDefuse = true;
force ace_explosives_punishNonSpecialists = true;
force ace_explosives_requireSpecialist = false;

// ACE Uncategorized
force ace_gforces_enabledFor = 1;
force ace_hitreactions_minDamageToTrigger = 0.1;
force ace_inventory_inventoryDisplaySize = 0;
force ace_laser_dispersionCount = 2;
force ace_microdagr_mapDataAvailable = 2;
force ace_optionsmenu_showNewsOnMainMenu = true;
force ace_overpressure_distanceCoefficient = 1;

// ACE Waffen
force ace_common_persistentLaserEnabled = false;
force ace_laserpointer_enabled = true;
force ace_reload_displayText = true;
force ace_weaponselect_displayText = true;

// ACE Wetter
force ace_weather_enabled = true;
force ace_weather_updateInterval = 60;
force ace_weather_windSimulation = true;

// ACE Windablenkung
force ace_winddeflection_enabled = true;
force ace_winddeflection_simulationInterval = 0.05;
force ace_winddeflection_vehicleEnabled = true;

// ACE Zeige Spielernamen
force ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
force ace_nametags_playerNamesMaxAlpha = 0.8;
force ace_nametags_playerNamesViewDistance = 5;
force ace_nametags_showCursorTagForVehicles = false;
force ace_nametags_showNamesForAI = false;
force ace_nametags_showPlayerNames = 0;
force ace_nametags_showPlayerRanks = false;
force ace_nametags_showSoundWaves = 0;
force ace_nametags_showVehicleCrewInfo = false;
force ace_nametags_tagSize = 2;

// ACE Zeus
force ace_zeus_autoAddObjects = false;
force ace_zeus_radioOrdnance = false;
force ace_zeus_remoteWind = false;
force ace_zeus_revealMines = 0;
force ace_zeus_zeusAscension = false;
force ace_zeus_zeusBird = false;

// ACE Zielfernrohre
force ace_scopes_correctZeroing = true;
force ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
force ace_scopes_defaultZeroRange = 100;
force ace_scopes_enabled = true;
force ace_scopes_forceUseOfAdjustmentTurrets = false;
force ace_scopes_overwriteZeroRange = false;
force ace_scopes_simplifiedZeroing = false;
force ace_scopes_useLegacyUI = false;
force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force ace_scopes_zeroReferenceHumidity = 0;
force ace_scopes_zeroReferenceTemperature = 15;

// ACE Zuschauer
force ace_spectator_enableAI = false;
force ace_spectator_restrictModes = 0;
force ace_spectator_restrictVisions = 0;

// ACE-Arsenal
force ace_arsenal_allowDefaultLoadouts = true;
force ace_arsenal_allowSharedLoadouts = true;
force ace_arsenal_camInverted = false;
force ace_arsenal_enableIdentityTabs = true;
force ace_arsenal_enableModIcons = true;
force ace_arsenal_EnableRPTLog = false;
force ace_arsenal_fontHeight = 4.5;

// ACE-Nachtsicht
force ace_nightvision_aimDownSightsBlur = 0.164407;
force ace_nightvision_disableNVGsWithSights = false;
force ace_nightvision_effectScaling = 0.223826;
force ace_nightvision_fogScaling = 0.194117;
force ace_nightvision_noiseScaling = 0.179264;
force ace_nightvision_shutterEffects = true;

// CBA UI
cba_ui_StorePasswords = 1;

// TFAR - Allgemeine Einstellungen
TFAR_AICanHearPlayer = false;
TFAR_AICanHearSpeaker = false;
TFAR_enableIntercom = true;
TFAR_fullDuplex = true;
TFAR_giveLongRangeRadioToGroupLeaders = false;
force TFAR_giveMicroDagrToSoldier = false;
TFAR_givePersonalRadioToRegularSoldier = false;
TFAR_globalRadioRangeCoef = 1;
TFAR_instantiate_instantiateAtBriefing = false;
TFAR_objectInterceptionEnabled = true;
tfar_radiocode_east = "_opfor";
tfar_radiocode_independent = "_independent";
force tfar_radiocode_west = "_independent";
tfar_radioCodesDisabled = false;
TFAR_SameLRFrequenciesForSide = false;
TFAR_SameSRFrequenciesForSide = false;
TFAR_setting_defaultFrequencies_lr_east = "";
TFAR_setting_defaultFrequencies_lr_independent = "";
TFAR_setting_defaultFrequencies_lr_west = "";
TFAR_setting_defaultFrequencies_sr_east = "";
TFAR_setting_defaultFrequencies_sr_independent = "";
TFAR_setting_defaultFrequencies_sr_west = "";
TFAR_setting_DefaultRadio_Airborne_east = "TFAR_mr6000l";
TFAR_setting_DefaultRadio_Airborne_Independent = "TFAR_anarc164";
TFAR_setting_DefaultRadio_Airborne_West = "TFAR_anarc210";
TFAR_setting_DefaultRadio_Backpack_east = "TFAR_mr3000";
TFAR_setting_DefaultRadio_Backpack_Independent = "TFAR_anprc155";
TFAR_setting_DefaultRadio_Backpack_west = "TFAR_rt1523g";
TFAR_setting_DefaultRadio_Personal_east = "TFAR_fadak";
TFAR_setting_DefaultRadio_Personal_Independent = "TFAR_anprc148jem";
TFAR_setting_DefaultRadio_Personal_West = "TFAR_anprc152";
TFAR_setting_DefaultRadio_Rifleman_East = "TFAR_pnr1000a";
TFAR_setting_DefaultRadio_Rifleman_Independent = "TFAR_anprc154";
TFAR_setting_DefaultRadio_Rifleman_West = "TFAR_rf7800str";
TFAR_spectatorCanHearEnemyUnits = true;
TFAR_spectatorCanHearFriendlies = true;
TFAR_takingRadio = 2;
TFAR_Teamspeak_Channel_Name = "TaskForceRadio";
TFAR_Teamspeak_Channel_Password = "123";
force tfar_terrain_interception_coefficient = 4.12953;

// TFAR - Benutzereinstellungen
TFAR_default_radioVolume = 7;
TFAR_intercomDucking = 0.2;
TFAR_intercomVolume = 0.3;
TFAR_moveWhileTabbedOut = false;
TFAR_oldVolumeHint = false;
TFAR_pluginTimeout = 4;
TFAR_PosUpdateMode = 0.1;
TFAR_showChannelChangedHint = true;
TFAR_showTransmittingHint = true;
TFAR_ShowVolumeHUD = false;
TFAR_tangentReleaseDelay = 0;
TFAR_VolumeHudTransparency = 0;
TFAR_volumeModifier_forceSpeech = false;
