/// Name of your mod
name = "Medic Bag - ACE3";
/// Author of the mod
author = "By-Jokese";
/// Picture displayed from the expansions menu/ Optimal size is 2048x1024, other sizes work too
picture = "FSG_MedicBag\ui\expansion.paa";
/// Display next to the item added by the mod
logoSmall = "FSG_MedicBag\ui\logo.paa";
/// Logo displayed in the main menu
logo = "FSG_MedicBag\ui\icon.paa";
/// When the mouse is over, in the main menu
logoOver = "FSG_MedicBag\ui\custom_patch_co.paa";
/// Website URL, that can accessed from the expansions menu
action = "https://frontsidegaming.com";
/// Tool tip displayed when the mouse is left over, in the main menu
tooltip = "Medic Bag - v1.3";
tooltipOwned = "Medic Bag - v1.3";
/// Color used for DLC stripes and backgrounds (RGBA)
dlcColor[] =
{
	1,
	0,
	0,
	1
};
/// Overview text, displayed from the extension menu
overview = "Medic Bag for ACE3 by Frontside Gaming [FSG]";
/// Hide the extension name
hideName = 0;
/// Hide the extension menu
hidePicture = 0;
