//regex
1 "" !=GroundWeaponHolder !=WeaponHolderSimulated !Supply[\d]{1,3} !=#lightpoint !=Land_ClutterCutter_small_F
5 "" !=GroundWeaponHolder !TFAR !tf_anprc155 !I_Heli_Transport_02_F !Arma_OPFOR_unarmed_A !B_Heli_Transport_03_unarmed_F !B_Slingload_01_Repair_F !I_soldier_UAV_F !tf_anarc164 !I_UAV_01_backpack_F !C_UAV_06_medical_F !I_UAV_AI !C_UAV_AI_F !Mustanglu_civ !AlessioMustang !RS6_Avantlu_GMK !Ducato !AlessioLaFerrari !Raptorlu_civ !chH1_ST !chRegalGNX !chCayenne !chH2 !GT500lu_civ !FFlu_civ !M5lu_civ !chTown_Car !cWrangler !AlessioR8 !Chironlu_civ !Wraithlu_civ !RS6_Avantlu_civ !fgtz3_civ !Alessio458GMK !AlessioAventador !CarlosHuracan !AlessioRS4 !ch250_GTO !chf_california !chTT_2 !AF_bmw_m6 !chCrossfire !Charger_1969lu_white !MP4lu_civ !SLSlu !V12_GHIBLI !P1lu_civ !Gallardolu_civ !AlessioRS4COP !AlessioMustangCOP !GT3POlu !AF_bmw_m6_P !chBrera !chBrera_bleu !chGiulietta !chMito !O_Truck_03_ammo_F !O_Truck_03_covered_F !B_Truck_01_transport_F !O_MRAP_02_F !B_MRAP_01_F !O_Truck_02_transport_F !I_Truck_02_covered_F !O_Truck_03_device_F !B_G_Van_01_transport_F !B_G_Van_02_vehicle_F !O_Quadbike_01_F !B_CTRG_LSV_01_light_F !C_Hatchback_01_F !C_Hatchback_01_sport_F !Mer_Vito_civ !chrr_svr !ch_sprinter_civ !chvwt5_civ !chClubman !chGrand_Cherokee !mondeo_civ !chH3 !Berlingo2_cciv !Land_Suitcase_F !B_RangeMaster_F !C_SUV_01_F !chRS2 !focussw3_civ !RS3lu_civ !chVol_850 !Tahoelu_civ !chgolf7_civ !chmeganers_civ !C_man_1 !passatb7_civch !chSmart !chcooper !C_Van_01_box_F !chciternvol_total !chtondeuse !Land_Money_F !plateau !chchariot !SmokeLauncherAmmo !chbalayeuse !chT1 !agora_tcl !Scania_124L !depanren !ap_Ducato !AF_bmw_m6_NEF !chVSAV_Sprinter !Transit_vtp !C_Van_02_medevac_F !I_UAV_01_F !C_UAV_06_medical_backpack_F !B_ViperLightHarness_blk_F !=WeaponHolderSimulated !EC635_SAR !C_IDAP_Van_02_medevac_F !C_Van_02_medevac_F !ace !AlessioRS4COP !Supply[\d]{1,3} !=#lightpoint !=Land_ClutterCutter_small_F !B_(OutdoorPack|AssaultPack|TacticalPack|Kitbag|FieldPack|Bergen|Carryall)_(blk|khk|dgtl|rgr|sgg|cbr|oli|mcamo|ocamo|oucamo|tna|ghex) !B_Parachute !B_Quadbike_01_F ![BOI]_Truck_0[123]_(box|covered|device|fuel|medical|transport)_F !C_Hatchback_01_(sport_)?F ![BC]_(G_)?Offroad_01_(armed_)?F !C_SUV_01_F !C_Van_01_(transport|box|fuel)_F !C_Kart_01_(Blu|Fuel|Red|Vrana)_F ![BCO]_Heli_Light_0[12]_(civil_|stripped_|unarmed_)?F !C_Rubberboat !C_Boat_Civil_01_(police_)?F !B_SDV_01_F ![BO]_MRAP_0[12]_(hmg_)?F !B_(Boat|Heli)_Transport_01_F !B_Boat_Armed_01_minigun_F !=C_Offroad_02_unarmed_F !=C_Plane_Civil_01_F !=C_Boat_Transport_02_F !=C_Scooter_Transport_01_F !=O_T_LSV_02_unarmed_F !=Chemlight_(red|yellow|green|blue) !=GrenadeHand_stone !=Land_(Razorwire|Money|Suitcase|CanisterFuel|Can|BottlePlastic|TacticalBacon)_F !=Land_Can_V3_F !=Land_BottlePlastic_V1_F !=B_RangeMaster_F !=C_man_1 !=Bo_GBU12_LGB_MI10 !=(CMflare_Chaff_|SmokeLauncher)Ammo !=Box_IND_Grenades_F !=B_supplyCrate_F 
5 B_AssaultPack_blk_DiverExp
5 B_AssaultPack_mcamo_AT
5 B_AssaultPack_mcamo_AA
5 B_AssaultPack_mcamo_AAR
5 B_AssaultPack_mcamo_Ammo
5 B_AssaultPack_rgr_LAT
5 B_AssaultPack_rgr_Medic
5 B_AssaultPack_rgr_Repair
5 B_AssaultPack_rgr_ReconMedic
5 B_AssaultPack_rgr_ReconExp
5 B_AssaultPack_rgr_ReconLAT
5 B_Kitbag_rgr_Exp
5 B_Kitbag_mcamo_Eng
5 B_FieldPack_ocamo_Medic
5 B_FieldPack_ocamo_AA
5 B_FieldPack_ocamo_AAR
5 B_FieldPack_ocamo_ReconMedic
5 B_FieldPack_ocamo_ReconExp
5 B_Carryall_ocamo_Exp
5 B_Carryall_mcamo_AAA
5 B_Carryall_mcamo_AAT
5 B_Carryall_ocamo_AAA
5 B_Carryall_ocamo_Eng
5 B_Carryall_cbr_AAT
5 B_Carryall_oucamo_AAT
5 B_Carryall_oucamo_AAA
5 B_Carryall_oucamo_Exp
5 B_Carryall_oucamo_Eng
5 B_B_Parachute_02_F

